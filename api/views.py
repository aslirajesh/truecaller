from django.utils.datastructures import MultiValueDictKeyError
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework import status
from accounts.models import ContactBook, Account
from api.serializers import ContactBookSerializer, \
    ContactBookSerializerAuth, ContactBookSerializerByName
from common.validate import *
import re


@api_view(['POST'])
@permission_classes([IsAuthenticated])
def mark_spam(request):
    """
    Mark a contact as spam
    :param request:
    :param : phone_number
    :return: json
    """
    try:
        if len(request.data) == 0:
            raise MultiValueDictKeyError
        if len(request.data) > 1:
            raise ValidationError('Only phone number is allowed')

        phone_number = request.data['phone_number']
        # validate phone number
        validate_phone_number(phone_number)
        contacts = ContactBook.objects.filter(phone_number=phone_number)
        # if contacts exists find all contacts with phone_number
        if contacts.exists():
            for contact in contacts:
                contact.is_spam = True
                contact.save()
            return Response({'phone_number': phone_number,
                             'status': 'success',
                             'message': 'Contact marked as spam'},
                            status=status.HTTP_200_OK)
        else:
            # if contacts does not exists create a new contact
            # and mark it as spam
            contact = ContactBook.objects.create(phone_number=phone_number,
                                                 is_spam=True)
            contact.save()
            return Response({'phone_number': phone_number,
                             'status': 'Success',
                             'message': 'Contact marked as spam'},
                            status=status.HTTP_201_CREATED)
    except MultiValueDictKeyError:
        return Response({'message': 'Please provide phone number',
                         'status': 'error'},
                        status=status.HTTP_400_BAD_REQUEST)
    except ValidationError as e:
        return Response({'message': e.args[0],
                         'status': 'error'},
                        status=status.HTTP_400_BAD_REQUEST)
    except Exception:
        return Response({'message': 'Something went wrong',
                         'status': 'error'},
                        status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def search_by_phone_number(request):
    """
    Search contact by phone number
    :param request:
    :param : phone_number
    :return: json
    """
    try:
        if len(request.data) == 0:
            raise MultiValueDictKeyError
        if len(request.data) > 1:
            raise ValidationError('Only phone number is allowed')

        phone_number = request.data['phone_number']
        # validate phone number
        validate_phone_number(phone_number)

        contact_list = []
        contacts = ContactBook.objects.filter(phone_number=phone_number)
        if contacts.exists():
            # change serializer based on request is authorised,
            # searched number is registered requester in contact book of person
            serializer = ContactBookSerializer(contacts, many=True)
            try:
                person = Account.objects.get(phone_number=phone_number)
                if request.user.is_authenticated:
                    person_contacts = ContactBook.objects.filter(
                        source_user=person.id,
                        phone_number=request.user.phone_number,
                        is_registered=True)

                    if person_contacts.exists():
                        serializer = ContactBookSerializerAuth(contacts,
                                                               many=True)
            except Account.DoesNotExist:
                pass
            # Append all contacts to contact_list
            for data in serializer.data:
                # if contact is registered clear list, add only one contact
                # and break the loop
                if data.get("is_registered"):
                    contact_list.clear()
                    contact_list.append(data)
                    break
                else:
                    contact_list.append(data)

            return Response({'message': 'Contact found',
                             'status': 'Success',
                             'contacts': contact_list},
                            status=status.HTTP_200_OK)
        else:
            return Response({'message': 'Contact not found',
                             'status': 'Success',
                             'contacts': contact_list},
                            status=status.HTTP_200_OK)

    except MultiValueDictKeyError:
        return Response({'message': 'Please provide phone number',
                         'status': 'error'},
                        status=status.HTTP_400_BAD_REQUEST)
    except ValidationError as e:
        return Response({'message': e.args[0],
                         'status': 'error'},
                        status=status.HTTP_400_BAD_REQUEST)
    except Exception as e:
        print(e)
        return Response({'message': 'Something went wrong',
                         'status': 'error'},
                        status=status.HTTP_500_INTERNAL_SERVER_ERROR)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def search_by_name(request):
    """
    Search contact by name
    :param request:
    :param : name
    :return: json
    """
    try:
        if len(request.data) == 0:
            raise MultiValueDictKeyError
        if len(request.data) > 1:
            raise ValidationError('Only name param is allowed')

        name = request.data['name']
        # validate name
        validate_name(name)

        contact_list = []
        contacts = ContactBook.objects.filter(name__icontains=name)

        if contacts.exists():
            serializer = ContactBookSerializerByName(contacts,
                                                     many=True)
            # Arrange in way that search name start with contact name
            for data in serializer.data:
                if re.match('^' + name + ' .*', data.get('name'),
                            re.IGNORECASE):
                    contact_list.append(data)

            for data in serializer.data:
                if re.match('^' + name + '.*', data.get('name'),
                            re.IGNORECASE):
                    contact_list.append(data)

            for data in serializer.data:
                if re.match('^(?!' + name + ').*', data.get('name'),
                            re.IGNORECASE):
                    contact_list.append(data)

            return Response({'message': 'Contacts found',
                             'status': 'Success',
                             'contacts': contact_list},
                            status=status.HTTP_200_OK)
        else:
            return Response({'message': 'Contacts not found',
                             'status': 'Success',
                             'contacts': contact_list},
                            status=status.HTTP_200_OK)

    except MultiValueDictKeyError:
        return Response({'message': 'Please provide search parameter',
                         'status': 'error'},
                        status=status.HTTP_400_BAD_REQUEST)


# for data populating in contact book
# for testing purpose
# can be removed
@api_view(['POST'])
@permission_classes([AllowAny])
def add_contact_book(request):
    """
    populate contact book with data
    can be used for adding data in contact book
    :param
    :param request:
    :return:
    """
    data = request.data
    contact = ContactBook.objects.create(phone_number=data['phone_number'],
                                         name=data['name'],
                                         email=data['email'],
                                         is_registered=False)
    contact.save()
    contact.source_user.add(data['source_user'])
    contact.save()
    return Response({'message': 'Contact added'},
                    status=status.HTTP_201_CREATED)
