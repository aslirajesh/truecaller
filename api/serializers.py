from djoser.serializers import UserCreateSerializer
from rest_framework import serializers
from accounts.models import Account, ContactBook


class CreateUserSerializer(UserCreateSerializer):
    class Meta(UserCreateSerializer.Meta):
        model = Account
        fields = ('id', 'phone_number', 'name')


class UserCreateTokenSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ('token',)


class ContactBookSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContactBook
        fields = ('phone_number', 'name', 'is_spam', 'is_registered')


class ContactBookSerializerAuth(serializers.ModelSerializer):
    class Meta:
        model = ContactBook
        fields = ('phone_number', 'name', 'email', 'is_spam', 'is_registered')


class ContactBookSerializerByName(serializers.ModelSerializer):
    class Meta:
        model = ContactBook
        fields = ('phone_number', 'name', 'is_spam')
