from django.urls import path, include
from api import views

urlpatterns = [
    path('', include('djoser.urls')),
    path('', include('djoser.urls.authtoken')),
    path('mark_spam', views.mark_spam, name='mark_spam'),
    path('search_by_number', views.search_by_phone_number,
         name='search_by_phone_number'),
    path('search_by_name', views.search_by_name, name='search_by_name'),
    path('add_contact', views.add_contact_book, name='add_contact'),
    ]
