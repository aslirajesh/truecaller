import accounts.models
from django.shortcuts import get_object_or_404


def add_registered_contact(user):
    """
    Add registered user to contacts list if not already exists.
    Make changes to the contact list if the user is already in the list.
    :param user:
    :return:
    """
    # check if user is spam
    is_this_user_spam = False
    try:
        this_user = accounts.models.ContactBook.objects.get(
            phone_number=user.phone_number)
        if this_user:
            if this_user.is_spam:
                is_this_user_spam = True
    except accounts.models.ContactBook.DoesNotExist:
        pass

    # Add new entry of register user to contact book
    contact = accounts.models.ContactBook(name=user.name,
                                          phone_number=user.phone_number,
                                          email=user.email,
                                          city=user.city,
                                          country=user.country,
                                          is_registered=True)
    contact.save()
    contact.source_user.add(user)
    contact.save()

    contacts = accounts.models.ContactBook.objects.filter(
        phone_number=user.phone_number)

    # Edit 'is_register' all entries of registered user to contact book
    # Edit 'is_spam' all entries of registered user to contact book
    for contact in contacts:
        contact.is_registered = True
        contact.is_spam = is_this_user_spam
        contact.save()
