from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import Account, ContactBook


class AccountAdmin(UserAdmin):
    list_display = ('phone_number', 'name', 'email', 'last_login',
                    'date_joined', 'is_active')
    list_display_links = ('email', 'name', 'phone_number')
    readonly_fields = ('last_login', 'date_joined')
    ordering = ('-date_joined',)

    filter_horizontal = ()
    list_filter = ()
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('name', 'phone_number', 'password1', 'password2'),
        }),
    )
    fieldsets = (
        (None, {'fields': ('phone_number', 'name', 'password')}),
        ('Personal info', {'fields': ('email', 'city', 'country')}),
        ('Permissions', {
            'fields': ('is_active', 'is_staff', 'is_admin', 'is_superadmin'),
        }),
        ('Important dates', {'fields': ('last_login', 'date_joined')}),
    )


class ContactAdmin(admin.ModelAdmin):
    list_display = ('phone_number', 'name', 'is_spam')


admin.site.register(ContactBook, ContactAdmin)
admin.site.register(Account, AccountAdmin)
