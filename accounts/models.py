from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
import accounts.views


class MyAccountManager(BaseUserManager):
    def create_user(self, name, email, phone_number=None, password=None):
        if not phone_number:
            raise ValueError("User must have an username")

        user = self.model(
            phone_number=phone_number,
            email=self.normalize_email(email),
            name=name
        )
        user.set_password(password)
        user.is_active = True
        user.save(using=self._db)
        accounts.views.add_registered_contact(user)
        return user

    def create_superuser(self, name, phone_number, email, password):
        user = self.create_user(
            name=name,
            email=email,
            phone_number=phone_number,
            password=password,
        )
        user.is_admin = True
        user.is_active = True
        user.is_staff = True
        user.is_superadmin = True
        user.save(using=self._db)
        return user


class Account(AbstractBaseUser):
    phone_number = models.CharField(max_length=10, unique=True, blank=False)
    name = models.CharField(max_length=100, blank=False)
    email = models.EmailField(max_length=100, unique=True)
    city = models.CharField(max_length=50, blank=True)
    country = models.CharField(max_length=50, blank=True)

    # required
    date_joined = models.DateTimeField(auto_now_add=True)
    last_login = models.DateTimeField(auto_now=True)
    is_admin = models.BooleanField(default=False)
    is_superadmin = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)

    USERNAME_FIELD = 'phone_number'

    REQUIRED_FIELDS = ['name', 'email', 'password']

    objects = MyAccountManager()

    def __str__(self):
        return str(self.id)

    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app_label):
        return True


class ContactBook(models.Model):
    name = models.CharField(max_length=200, blank=True)
    phone_number = models.BigIntegerField(blank=False, unique=False)
    email = models.EmailField(max_length=100, blank=True)
    city = models.CharField(max_length=50, blank=True)
    country = models.CharField(max_length=50, blank=True)
    is_registered = models.BooleanField(default=False)
    is_spam = models.BooleanField(default=False)
    source_user = models.ManyToManyField(Account)
    objects = models.Manager()

    def __str__(self):
        return self.name
