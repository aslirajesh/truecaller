from rest_framework.exceptions import ValidationError
import re

name_regex = re.compile(r'^[a-zA-Z\-_]+$')


def validate_phone_number(phone_number):
    """
    Validate phone number
    :param phone_number:
    :return:
    """
    if phone_number:
        if len(phone_number) == 10:
            if phone_number.isdigit():
                return True
            else:
                raise ValidationError('Phone number must be numeric')
        else:
            raise ValidationError('Phone number must be 10 digits')
    else:
        raise ValidationError('Phone number is required')


def validate_name(name):
    """
    Validate name
    :param name:
    :return:
    """
    if name:
        if len(name) > 0:
            if re.match(r'^[a-zA-Z \-_]+$', name):
                return True
            else:
                raise ValidationError('Name must be alphabetic')
        else:
            raise ValidationError('Name is required')
    else:
        raise ValidationError('Name is required')
