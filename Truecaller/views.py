from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from rest_framework import status
from datetime import datetime


@api_view(['GET'])
@permission_classes([AllowAny])
def index(request):
    date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    message = 'Server is running Current Time is :'
    return Response({'data': message + date, 'host': request.META['HTTP_HOST']
                     }, status=status.HTTP_200_OK)
